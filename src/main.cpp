
#include <ArduinoWebsockets.h>
#include <WiFi.h>
#include <arduino-timer.h>
#ifndef ESP32
#pragma message(THIS EXAMPLE IS FOR ESP32 ONLY!)
#error Select ESP32 board.
#endif

using namespace websockets;
/** Wifi part           */
char* ssid = "derstrudel.org";
char* password =  "";
bool connected=false;
/** websocket part */
const uint16_t port =3000;
//139.6.56.123
//strudel 168.119.230.60
const char * host = "192.168.1.23";
WebsocketsClient ws_client;

//bicore definition
const int NUM_BICORES = 6;

float activity[NUM_BICORES];
float low_pass[NUM_BICORES];
float tau=50;



int analog_ports[] = {36 ,39,34, 35, 32, 33};
int minima[NUM_BICORES];
int maxima[NUM_BICORES];

int sampling_counter = 0;

int value_to_send[NUM_BICORES];
/* Flag if main loop is running */
/* microcontroller setup, only exeduted once */
const int MOTION_PIN_IN=17;
const int MOTION_PIN_OUT=22;
const int CALIBRATE_PIN=16;


//auto timer_on = timer_create_default(); // create a timer with default settings
Timer<> timer_on; // save as above

boolean installation_on = false;

bool set_on(void *) {
  Serial.print("print_message: ");
  //Serial.println(m);
  installation_on=false;
  return true; // repeat? true
}


void setup()
{
        /* serial communication for debugging purposes*/
        Serial.begin(115200);
        Serial.print("Connecting to ");
        Serial.println(ssid);
        /*connecting to the WIFI */
        WiFi.begin(ssid, password);
	WiFi.setSleep(WIFI_PS_NONE);
        while (WiFi.status() != WL_CONNECTED) {
                delay(500);
                Serial.println("trying to connect...");
        }
        Serial.print("WiFi connected with IP: ");
        Serial.println(WiFi.localIP());
        for (int i = 0; i < NUM_BICORES; i++) {
	  minima[i]=10000;
	  maxima[i]=0;
	  value_to_send[i]=0;
        }
	sampling_counter=0;
	pinMode(MOTION_PIN_IN,INPUT);
	pinMode(MOTION_PIN_OUT,OUTPUT);
	pinMode(CALIBRATE_PIN,INPUT);
	// call the toggle_led function every 500 millis (half second)
	//timer_on.every(500, toggle_led);
	
}

void loop()
{
  /** connect to remote server for websocket communication */
  if (!connected) {
    if (!ws_client.connect(host, port,"/?ESP32")) {
      Serial.println("Connection to host failed");
      delay(1000);
      return;
    }
    else
      {
	Serial.println("Connected to server successful!");
	connected=true;
      }
  }
  if (connected)
    {
      for (int i = 0; i < NUM_BICORES; i++)
	{
	  activity[i] = analogRead(analog_ports[i]);
	}
      boolean motion= digitalRead(MOTION_PIN_IN);
      if (motion&&!installation_on) {
	//timer_on.in(2*60000, set_on);
	timer_on.in(2*6000, set_on);
	installation_on=true;
	}
      Serial.print(installation_on);
      Serial.print(",");
      Serial.println(motion);
      //      digitalWrite( MOTION_PIN_OUT, 0);
      boolean calibrate=digitalRead(CALIBRATE_PIN);
      if (calibrate) {
        sampling_counter = 10000;
      }
            digitalWrite(MOTION_PIN_OUT,installation_on);
      String data="";
      for (int i = 0; i < NUM_BICORES; i++)
	{
	  low_pass[i] += 1 / tau * (-low_pass[i] + activity[i]);
	  if ((sampling_counter<20000)&&(sampling_counter>10000))
	    {
	      if (low_pass[i] < minima[i]) {
		minima[i]=low_pass[i];
	      }
	      if (low_pass[i] > maxima[i]) {
		maxima[i] = low_pass[i];
	      }
	    }
	  if (sampling_counter<20000)
	    sampling_counter++;
          else {
	    if (abs(maxima[i]-minima[i])>1)
	      value_to_send[i]=1000./(maxima[i]-minima[i])  *(low_pass[i] -minima[i]);
	    else
	      value_to_send[i]=1000  *(low_pass[i] -minima[i]);
          }
          if (i<NUM_BICORES-1)
	    data+=String(value_to_send[i])+",";
	  else
	    data+=String(value_to_send[i]);
	}
      Serial.println(data);
      timer_on.tick(); // tick the timer
      //send to websocket server
      ws_client.send(data.c_str());
    }
}
