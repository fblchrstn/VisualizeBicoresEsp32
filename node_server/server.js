//express provides a simple http server
const express = require('express');
//ws is a simple websocket package
const ws = require('ws');
//webserver setup
const app = express();
app.use(express.static('public'));
//   websocket server 
const wsServer = new ws.Server({ noServer: true });

var lastMessageEsp32="0,0,0";
var lastMessageUserInput=0;
//clients list
var clients = [[],[],[]];

//array to store socket conenctions
var clientsSockets=[[],[],[]];
let next=true;
//on first connection the request url is used to identify the differnt clients
wsServer.on('connection', (socket,request) => {
    console.log(request.url);
    if (request.url=="/?ESP32")
    {
	console.log("connect from esp");
	//the websocket key provides a unique identifier
	console.log(clients[0]);
	clients[0].push(request.headers['sec-websocket-key']);
	console.log(clients[0]);
	clientsSockets[0].push(socket);
    }
    if (request.url=="/?display")
    {
	console.log("connect from visualization frontend");
	console.log(clients[1]);
	console.log(request.headers['sec-websocket-key']);
	clients[1].push(request.headers['sec-websocket-key']);
	console.log(clients[1]);
	for (i=0;i<clients[1].length;i++)
	{
	    console.log(clients[1][i]);
	}
	clientsSockets[1].push(socket);
    }

    socket.id = request.headers['sec-websocket-key'];
    //on incoming messages 
    socket.on('message', message => {
	//index 0 is the esp32 client
	if (socket.id==clients[0][0])
	{
	    /*if the visualization client (index 1) is connected the 
	      message received form the esp32 is forwared to the visualization client*/
	    //	    console.log(clientsSockets.length);
	  //  console.log(message+",");
	    if (next)
	    {
		for (i=0;i<clientsSockets[1].length;i++)
		{
		    clientsSockets[1][i].send(message+",");
		}
		lastMessageEsp32=message;
		next=false;
	    }
	    
//	    console.log(message+",");
	}
	for (i=0;i<clients[1].length;i++)
	{
	    if (socket.id==clients[1][i])
	    {
		next=true;
	    }
	}
    });
    
});

//the webserver listening on port 3000
const server = app.listen(3000);
server.on('upgrade', (request, socket, head) => {
  wsServer.handleUpgrade(request, socket, head, socket => {
    wsServer.emit('connection', socket, request);
  });
});
