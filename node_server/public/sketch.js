// where the serial server is (your local machine):
let host = '192.168.1.117:3000/?display';
let socket; // the websocket
let sensorValues = []; // the sensor value
let stepX=1;
let tile =[];
let numTilesX=5;
let numTilesY=3;
let bufferSize=200;
let drawing=false
function setup() {
    canvas=createCanvas(displayWidth-100, displayHeight-100);
    canvas.parent("canvas");
    // connect to server:
    socket = new WebSocket('ws://' + host);
    // socket connection listener:
    socket.onopen = sendIntro;
    // socket message listener:
    socket.onmessage = readMessage;
    background(255);   
    for (let i=0;i<6;i++)
    {
	sensorValues.push(0);
    }
    let i=0;
    for (y=0;y<numTilesY;y++)
	for (x=0;x<numTilesX;x++)
    {
	tile[i]=new DrawTile(x*width/numTilesX,y*height/numTilesY+50,width/(numTilesX+1),height/numTilesY,bufferSize);
	i++;
    }
    noLoop();
}

function draw() {
    drawing=true;
    background(0);
    fill(255,240);
    let i=0;
    for (let y=0;y<numTilesY;y++)
    {
	for (let x=0;x<numTilesX;x++)
	{
	    let index_value_y = y ;
	    let index_value_x = x+1;
	    if (index_value_x==index_value_y)
	    {
		index_value_y+=1;
	    }		
	    tile[i].draw(sensorValues[index_value_x],sensorValues[index_value_y]);
	    noStroke();
	    text(index_value_x+" "+index_value_y,tile[i].x_position+20,tile[i].y_position+20);
	    i++;
	}
    }
    for (let j=0;j<tile.length;j++)
    {
	for (let k=0;k<tile[0].draw_buffer_size;k++)
	{
	    strokeWeight(3);
	    //let which = k % tile[j].draw_buffer_size;
	    let index = k;//(which+1 + k) % tile[j].draw_buffer_size;
	    stroke(255);
	    stroke(255,10+150.0*index/(1.0*tile[j].draw_buffer_size));
	    
	    if (index==0)
	    {
		index++;
	    }
	    
	    line(tile[j].x_position+tile[j].draw_buffer_x[index-1], tile[j].y_position+tile[j].draw_buffer_y[index-1], tile[j].x_position+tile[j].draw_buffer_x[index], tile[j].y_position+tile[j].draw_buffer_y[index]);
	}
    }
    for (let i=0;i<numTilesY*numTilesX;i++)
    {
	stroke(255,0,0,120);
	fill(255,0,0,120);
	ellipse(tile[i].draw_buffer_x[tile[i].draw_buffer_x.length-1]+tile[i].x_position,tile[i].draw_buffer_y[tile[i].draw_buffer_y.length-1]+tile[i].y_position,20,20);
	noFill();
    }
    drawing=false;
}


function sendIntro() {
    // convert the message object to a string and send it:
    socket.send("next");
}
function readMessage(event) {
    var msg = event.data; // read data from the onmessage event
    elements=splitTokens(msg,",");
    for (let i=sensorValues.length;i<elements.length;i++)
    {
	sensorValue.push(0);
    }
    for (let i = 0;i < elements.length; i++) {
	sensorValues[i] = Number(elements[i]);
    }
    if (!drawing)
    {
	draw();
	socket.send("next");
    }
    
}

function CircularArray(maxLength) {
  this.maxLength = maxLength;
}

CircularArray.prototype = Object.create(Array.prototype);

CircularArray.prototype.push = function(element) {
  Array.prototype.push.call(this, element);
  while (this.length > this.maxLength) {
    this.shift();
  }
}

class DrawTile
{
    constructor(x_position,y_position,width,height,draw_buffer_size)
    {
	this.x_position=x_position;
	this.y_position=y_position;
	this.width=width;
	this.height=height;
	this.draw_buffer_size=draw_buffer_size;
	this.draw_buffer_x=new CircularArray([draw_buffer_size]);
	this.draw_buffer_y=new CircularArray([draw_buffer_size]);
	this.drawCounter=0;
	this.margin=50;
    }
    draw(x_data,y_data){
	stroke(255,0,0,120);
//	rect(this.x_position,this.y_position,this.width,this.height);
	if (x_data)
	    x_data=map(x_data,0,1000,this.margin,this.width-this.margin);
	this.draw_buffer_x.push(x_data);
	if (y_data)
	    y_data=map(y_data,0,1000,this.height-this.margin,this.margin);
	this.draw_buffer_y.push(y_data);
    }
}

socket.onclose = function (e){
    console.log("connection closed");
};
window.addEventListener("unload", function () {
    if(socket.readyState == WebSocket.OPEN)
        socket.close();
});
