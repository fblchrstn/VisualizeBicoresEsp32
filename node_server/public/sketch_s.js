// where the serial server is (your local machine):
let host = '192.168.205.114:3000/?display';
let socket; // the websocket
let sensorValues = []; // the sensor value
let tempVector = [];
let stepX=10;
function setup() {
    canvas=createCanvas(400, 400);
    canvas.parent("canvas");
  // connect to server:
  socket = new WebSocket('wss://' + host);
  // socket connection listener:
  socket.onopen = sendIntro;
  // socket message listener:
  socket.onmessage = readMessage;
    background(255);   
    for (let i=0;i<3;i++)
    {
	sensorValues.push(0);
	tempVector[i]=new CircularArray(width/stepX);
    }
}

function draw() {
    colorMode(RGB);
    background(255);
    fill(255,0,0,240);
    colorMode(HSB,360,100,100);
    for (let i=0;i<tempVector.length;i++)
    {
	stroke(360/tempVector.length*i,100,100);
	for (let x=0; x<tempVector[i].length-1;x++)
	{
	    line(x*stepX,height-tempVector[i][x],(x+1)*stepX,height-tempVector[i][x+1])
	//text(sensorValues[i][, 20, 20);
	}
//	text(tempVector[i][tempVector[i].length-1],(i+1)*20,20);
     }
 }


function sendIntro() {
  // convert the message object to a string and send it:
  socket.send("Hallo");
}
function readMessage(event) {
    var msg = event.data; // read data from the onmessage event
    console.log(msg);
    elements = splitTokens(msg, ",");
    if (elements.length>sensorValues.length)
    {
	console.log("adding element");
	sensorValues.push(0);
	tempVector.push(new CircularArray(width/stepX));
    }
  for (var i = 0;i < elements.length; i++) {
      sensorValues[i] = 1*(float)(elements[i]);
      if (i==0)
	  tempVector[i].push(map(sensorValues[i],0,40,0,height));
      if (i==1)			     
          tempVector[i].push(map(sensorValues[i],0,100,0,height));
      if (i==2)
	  tempVector[i].push(map(sensorValues[i],0,4095,0,height));
      if (i>2)
	  tempVector[i].push(map(sensorValues[i],0,100,0,height));
   }
    console.log(elements.length);
  //println(sensorValue); // print it
}
function mouseClicked()
{
    sendIntro();
}

function CircularArray(maxLength) {
  this.maxLength = maxLength;
}

CircularArray.prototype = Object.create(Array.prototype);

CircularArray.prototype.push = function(element) {
  Array.prototype.push.call(this, element);
  while (this.length > this.maxLength) {
    this.shift();
  }
}
